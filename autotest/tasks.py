from AutoTestFW import celery_app
from time import sleep
import json
import xmlrpclib
from . import models
from .models import TestHost
import logging
import string
from utils.monitor import *
from utils.monitor import TimeoutTransport


logger = logging.getLogger('autotest.tasks')
CPU_THRESHOLD = 80


@celery_app.task()
def keep_alive():
    # set the timeout = 3s
    t = TimeoutTransport()
    t.set_timeout(3)
    allhosts = TestHost.objects.all()
    for host in allhosts:
        address = "http://" + host.ip + ":8008/"
        try:
            proxy = xmlrpclib.ServerProxy(address, transport=t)
            result = proxy.keep_alive()
            if result == "alive":
                logger.info("Host %s is alive", host.ip)
            else:
                logger.error("Error msg received from host %s", host.ip)
                mail_conn_error(host.ip)
                lock_host(host.ip)
        except Exception as e:
            logger.error(e)
            logger.error("Host %s is dead ", host.ip)
            mail_conn_error(host.ip)
            lock_host(host.ip)


@celery_app.task()
def schedule_task_to_docker():
    pass


@celery_app.task()
def schedule_task_to_vm(msgjson):
    # Message with JSON format
    msgdict = json.loads(msgjson)
    ostype = msgdict["osType"]
    #  testHostId = msgdict["testHostId"]
    hostip = msgdict['testHostIp']
    vmjob = VmJobProcess(ostype)
    retrynum = 60  # retry 60 times when failing to reserve host, wait 1 minute everytime before retrying
    while retrynum > 0:
        targethostip = None
        if hostip is None:
            # Pick test host automatically
            targethostip = vmjob.get_target_vm()
        else:
            targethostip = vmjob.get_assigned_vm(hostip)
        if not targethostip or not vmjob.find_vm_state():
            if retrynum == 1:
                raise Exception("No available test host found!")
            retrynum -= 1
            sleep(30)
            continue
        break
    if vmjob.find_vm_state():
        targethostId = TestHost.objects.get(ip=targethostip).id
        vmjob.set_target_host(targethostId, targethostip)
        mail_new_job(jobMsg=msgdict)
        vmjob.send_job(targethostip=targethostip, msgjson=msgjson)


class VmJobProcess(object):
    def __init__(self, ostype):
        self.ostype = ostype
        self.VM_FOUND = True
        self.targetHost = {}

    def set_target_host(self, id, ip):
        self.targetHost = {'id': id, 'ip': ip}

    def find_vm_state(self):
        return self.VM_FOUND

    def get_assigned_vm(self, hostip):
        tmphostip = None
        state = TestHost.objects.get(ip=hostip).status
        if state == "0" and self._get_cpu_usage(hostip) < CPU_THRESHOLD:
            tmphostip = hostip
            logger.info("Successfully reserved host IP: %s", tmphostip)
        else:
            self.VM_FOUND = False
        return tmphostip

    def get_target_vm(self):
        cpuvalue = {}
        hostiplist = {}
        tmphostip = None
        # Step1. Get all the unlocked VMs' cpu usage
        hosts = self._filter_unlocked_hosts(str(self.ostype))

        for host in hosts:
            # cpuvalue --> {"1": ("172.16.24.112", "30"),  "2":
            # ("172.24.16.113", "100"), }
            cpuvalue[host.id] = self._get_cpu_usage(host.ip)
            hostiplist[host.id] = host.ip

        # Step2. Filter the VM with lowest cpu usage, but < threshold
        if not cpuvalue:
            logger.warn("No test host available found within CPU usage <80%!")
            logger.warn("Will try it later")
            self.VM_FOUND = False
        else:
            tmphost = min(cpuvalue.items(), key=lambda x: x[1])
            tmphostid, tmphostcpu = tmphost[0], tmphost[1]
            if string.atof(tmphostcpu) >= CPU_THRESHOLD:
                logger.error("CPU usage is %s >= 80", tmphostcpu)
                logger.warn("Will try it later")
                self.VM_FOUND = False
            else:
                tmphostip = hostiplist[tmphostid]
                logger.info("Successfully reserved host IP: %s", tmphostip)
        return tmphostip

    def send_job(self, targethostip, msgjson):
        retry = 1
        address = "http://" + targethostip + ":8008/"
        while retry > 0:
            try:
                proxy = xmlrpclib.ServerProxy(address)
                resultjson = proxy.run_job(msgjson)
                self.insert_result_to_db(resultjson, msgjson)
                break
            except Exception as e:
                logger.error(e)
                logger.error(
                    "Error occurred when running job on host: ", targethostip)
                resultjson = ""
                retry -= 1
        return resultjson

    def insert_result_to_db(self, resultjson, msg):
        testResult = json.loads(resultjson)
        passNum = testResult['passNum']
        failNum = testResult['failNum']
        errorNum = testResult['errorNum']
        startTime = testResult['startTime']
        endTime = testResult['endTime']
        executeTime = testResult['executeTime']
        msgResult = json.loads(msg)
        testResultId = msgResult['testResultId']
        submittime = testResultId.split('_')[2]
        suiteNameId = msgResult['suiteId']
        testHostId = self.targetHost['id']
        sutId = msgResult['sutId']
        release = msgResult['release']
        browser = msgResult['browser']
        productId = msgResult['productId']
        planNameId = msgResult['planId']
        total = msgResult['total']
        fulfill = msgResult['fulfill']
        report_path = 'ftp://172.16.24.110/pub/log/' + testResult['reportPath']
        models.TestResult.objects.create(testresult_ID=testResultId, submittime=submittime, release=release, passnum=passNum,
                                         failnum=failNum, errornum=errorNum, starttime=startTime, endtime=endTime, executetime=executeTime, browser=browser, plan_name_id=planNameId,
                                         suite_name_id=suiteNameId, sut_ip_id=sutId, testhost_ip_id=testHostId,
                                         product_name_id=productId, report_path=report_path, total=total, fulfill=fulfill)

    @staticmethod
    def _filter_unlocked_hosts(ostype):
        # Filter out all hosts with matched os type and the status is unlocked
        return TestHost.objects.filter(status="0", os_type=ostype)

    def _get_cpu_usage(self, hostip):
        retry = 3
        address = "http://" + hostip + ":8008/"
        while retry > 0:
            try:
                proxy = xmlrpclib.ServerProxy(address)
                result = proxy.get_cpu()
                break
            except Exception as e:
                # the cpu usage set to 100 once any error happened here
                logger.error(e)
                logger.error(
                    "Error occurred when getting cpu usage for ", hostip)
                retry -= 1
        if retry == 0:
            # Set the host to lock state in DB
            result = 100
            lock_host(hostip=hostip)
        return result


def lock_host(hostip):
    try:
        host = TestHost.objects.get(ip=hostip)
        host.status = "1"
        host.save()
    except Exception as e:
        logger.warn("Unable to lock the Host with IP:", hostip)
        logger.info("Exception : ", e)
