from django.contrib import admin
from models import Product
from models import TestSuite
from models import TestHost
from models import SUT
from models import TestPlan
from models import TestResult
# Register your models here.
admin.site.register(Product)
admin.site.register(TestSuite)
admin.site.register(TestHost)
admin.site.register(SUT)
admin.site.register(TestPlan)
admin.site.register(TestResult)