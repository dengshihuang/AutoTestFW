# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from . import models
from django.http import HttpResponseRedirect, HttpResponse
from django.core import serializers
import logging
from django.conf import settings
from utils.jsonify import jsonify
import json
import datetime
from django.db.models import Q, Max
from .tasks import schedule_task_to_vm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
logger = logging.getLogger('autotest.views')
import time
import functools

# Create your views here.


# 编辑产品

def product_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            products = models.Product.objects.select_related().all().order_by('id')
            total = products.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            paginator = Paginator(products, limit)
            contacts = paginator.page(page)
            logger.info(products)
            re_data = serializers.serialize("json", contacts)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_PRODUCT_SELECT,
                   "msg": "products select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_PRODUCT,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def product_delete(request, product_id):
    res = {'code': 0, 'data': ''}
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            target = models.Product.objects.get(pk=product_id)
            if target:
                logger.debug("delete", target.name)
                models.Product.objects.filter(pk=product_id).delete()
                res = {"code": 0, 'data': target.name}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "products not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "product not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def product_edit(request, product_id):
    if request.method == 'POST':
        product_name = request.POST.get('name')
        if product_name == "" or product_name is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'product name cannot be null'}
            return HttpResponse(jsonify(res))
        if product_id.isdigit():
            try:
                product = models.Product.objects.get(pk=product_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'product not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            product.name = product_name
            if models.Product.objects.filter(~Q(id=product_id), name=product_name):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'product already exists'}
                return HttpResponse(jsonify(res))
            product.save()
            res = {'code': 0, 'productName': product_name}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'product_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def product_add(request):
    if request.method == 'POST':
        product_name = request.POST.get('name')
        if models.Product.objects.filter(name=product_name):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'product already exists'}
            return HttpResponse(jsonify(res))
        if product_name is None:
            res = {'code': settings.ERR_PARAM, 'msg': 'param can not be null '}
            return HttpResponse(jsonify(res))
        models.Product.objects.create(name=product_name)
        res = {'code': 0, 'productName': product_name}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))

# module


def module_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            modules = models.Module.objects.order_by(
                'id').select_related().all().order_by('id')
            total = modules.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            paginator = Paginator(modules, limit)
            contacts = paginator.page(page)
            logger.info(modules)
            re_data = serializers.serialize("json", contacts)

            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_PRODUCT_SELECT,
                   "msg": "modules select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_PARAM,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def module_delete(request, module_id):
    res = {'code': 0, 'data': ''}
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            target = models.Module.objects.get(pk=module_id)
            if target:
                logger.debug("delete", target.name)
                models.Module.objects.filter(pk=module_id).delete()
                res = {"code": 0, 'data': target.name}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "modules not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "module not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def module_edit(request, module_id):
    if request.method == 'POST':
        req = json.loads(request.body)
        module_name = req['name']
        product_id = req['product_name_id']
        if module_name == "" or module_name is None and\
                product_id is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'module name cannot be null'}
            return HttpResponse(jsonify(res))
        if module_id.isdigit():
            try:
                module = models.Module.objects.get(pk=module_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'module not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            if models.Module.objects.filter(~Q(id=module_id), name=module_name, product_name_id=product_id):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'module already exists'}
                return HttpResponse(jsonify(res))
            module.name = module_name
            module.product_name_id = product_id
            module.save()
            res = {'code': 0, 'moduleName': module_name}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'module_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def module_add(request):
    if request.method == 'POST':
        req = json.loads(request.body)
        module_name = req['name']
        product_id = req['product_name_id']
        if models.Module.objects.filter(name=module_name):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'module already exists'}
            return HttpResponse(jsonify(res))
        if module_name is None:
            res = {'code': settings.ERR_PARAM, 'msg': 'param can not be null '}
            return HttpResponse(jsonify(res))
        models.Module.objects.create(
            name=module_name, product_name_id=product_id)
        res = {'code': 0, 'moduleName': module_name}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


# release
def release_getReleaseByProductId(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        product_id = request.GET.get('product_name_id')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            releases = models.Release.objects.filter(
                product_name_id=product_id)
            total = releases.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            paginator = Paginator(releases, limit)
            contacts = paginator.page(page)
            logger.info(releases)
            re_data = serializers.serialize("json", contacts)

            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_RELEASE_SELECT_BY_PRODUCTID,
                   "msg": "modules select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_PARAM,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


# 编辑测试机


def testHost_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            testHosts = models.TestHost.objects.select_related().all().order_by('id')
            total = testHosts.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            curentPage = Paginator(testHosts, limit).page(page)
            logger.info(curentPage)
            re_data = serializers.serialize("json", curentPage)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_TESTHOST_SELECT,
                   "msg": "testHosts select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,
               "msg": 'request method error, plese use get', 'meta': "%s" % e}
        return HttpResponse(jsonify(res))


def testHost_delete(request, testHost_id):
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            testHost = models.TestHost.objects.get(id=testHost_id)
            if testHost:
                logger.debug("delete", testHost.ip)
                models.TestHost.objects.filter(id=testHost_id).delete()
                res = {"code": 0, 'testHostName': testHost.ip}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "testHost not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "testHost not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def testHost_edit(request, testHost_id):
    if request.method == 'POST':
        ip = request.POST.get('ip')
        user = request.POST.get('user')
        password = request.POST.get('password')
        status = request.POST.get('status')
        use_by = request.POST.get('use_by')
        os_type = request.POST.get('os_type')

        if str(ip).strip() == "" or ip is None or \
                str(user).strip() == "" or user is None or \
                str(password).strip() == "" or password is None or \
                str(status).strip() == "" or status is None or \
                str(os_type).strip() == "" or os_type is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if status == '0' and not use_by is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'when status is idel, use_by must be null'}
            return HttpResponse(jsonify(res))
        if testHost_id.isdigit():
            try:
                testHost = models.TestHost.objects.get(pk=testHost_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'testHost not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            if models.TestHost.objects.filter(~Q(id=testHost_id), ip=ip):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'testHost already exists'}
                return HttpResponse(jsonify(res))
            testHost.ip = ip
            testHost.user = user
            testHost.password = password
            testHost.status = status
            testHost.use_by = use_by
            testHost.os_type = os_type
            testHost.save()
            res = {'code': 0, 'testHostIp': ip}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'testHost_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def testHost_add(request):
    if request.method == 'POST':
        ip = request.POST.get('ip')
        user = request.POST.get('user')
        password = request.POST.get('password')
        status = request.POST.get('status')
        use_by = request.POST.get('use_by')
        os_type = request.POST.get('os_type')
        if str(ip).strip() == "" or ip is None or \
                str(user).strip() == "" or user is None or \
                str(password).strip() == "" or password is None or \
                str(status).strip() == "" or status is None or \
                str(os_type).strip() == "" or os_type is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if status == '0' and not use_by is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'when status is idel, use_by must be null'}
            return HttpResponse(jsonify(res))
        if models.TestHost.objects.filter(ip=ip):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'testHost already exists'}
            return HttpResponse(jsonify(res))
        models.TestHost.objects.create(
            ip=ip, user=user, password=password, status=status, use_by=use_by, os_type=os_type)
        res = {'code': 0, 'testHostIp': ip}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


# 编辑服务机


def sut_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            suts = models.SUT.objects.select_related().all().order_by('id')
            total = suts.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            curentData = Paginator(suts, limit).page(page)
            logger.info(suts)
            re_data = serializers.serialize("json", curentData)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_SUT_SELECT,
                   "msg": "suts select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def sut_delete(request, sut_id):
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            sut = models.SUT.objects.get(pk=sut_id)
            if sut:
                logger.debug("delete", sut.ip)
                models.SUT.objects.filter(id=sut_id).delete()
                res = {"code": 0, 'sutName': sut.ip}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "sut not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "sut not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def sut_edit(request, sut_id):
    if request.method == 'POST':
        ip = request.POST.get('ip')
        user = request.POST.get('user')
        password = request.POST.get('password')
        status = request.POST.get('status')
        use_by = request.POST.get('use_by')
        os_type = request.POST.get('os_type')

        if str(ip).strip() == "" or ip is None or \
                str(user).strip() == "" or user is None or \
                str(password).strip() == "" or password is None or \
                str(status).strip() == "" or status is None or \
                str(os_type).strip() == "" or os_type is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if status == '0' and not use_by is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'when status is idel, use_by must be null'}
            return HttpResponse(jsonify(res))
        if sut_id.isdigit():
            try:
                sut = models.SUT.objects.get(pk=sut_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'sut not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            if models.SUT.objects.filter(~Q(id=sut_id), ip=ip):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'sut already exists'}
                return HttpResponse(jsonify(res))
            sut.ip = ip
            sut.user = user
            sut.password = password
            sut.status = status
            sut.use_by = use_by
            sut.os_type = os_type
            sut.save()
            res = {'code': 0, 'sutIp': ip}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'sut_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def sut_add(request):
    if request.method == 'POST':
        ip = request.POST.get('ip')
        user = request.POST.get('user')
        password = request.POST.get('password')
        status = request.POST.get('status')
        use_by = request.POST.get('use_by')
        os_type = request.POST.get('os_type')
        if str(ip).strip() == "" or ip is None or \
                str(user).strip() == "" or user is None or \
                str(password).strip() == "" or password is None or \
                str(status).strip() == "" or status is None or \
                str(os_type).strip() == "" or os_type is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if status == '0' and not use_by is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'when status is idel, use_by must be null'}
            return HttpResponse(jsonify(res))
        if models.SUT.objects.filter(ip=ip):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'sut already exists'}
            return HttpResponse(jsonify(res))
        models.SUT.objects.create(
            ip=ip, user=user, password=password, status=status, use_by=use_by, os_type=os_type)
        res = {'code': 0, 'sutIp': ip}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


# 编辑用例


def testSuite_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            testSuites = models.TestSuite.objects.select_related().all().order_by('id')
            total = testSuites.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            curentData = Paginator(testSuites, limit).page(page)
            logger.info(testSuites)
            re_data = serializers.serialize("json", curentData)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_TESTSUITE_SELECT,
                   "msg": "testSuites select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def testSuite_delete(request, testSuite_id):
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            testSuite = models.TestSuite.objects.get(pk=testSuite_id)
            if testSuite:
                logger.debug("delete", testSuite.name)
                models.TestSuite.objects.filter(pk=testSuite_id).delete()
                res = {"code": 0, 'testSuiteName': testSuite.name}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "testSuite not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "testSuite not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def testSuite_edit(request, testSuite_id):
    if request.method == 'POST':
        name = request.POST.get('name')
        product_name_id = request.POST.get('product_name_id')
        suite_type = request.POST.get('suite_type')
        module = request.POST.get('module_name_id')
        owner = request.POST.get('owner')
        level = request.POST.get('level')

        if str(name).strip() == "" or name is None or \
                str(product_name_id).strip() == "" or product_name_id is None or \
                str(suite_type).strip() == "" or suite_type is None or \
                str(module).strip() == "" or module is None or \
                str(level).strip() == "" or level is None or \
                str(owner).strip() == "" or owner is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))

        if testSuite_id.isdigit():
            try:
                testSuite = models.TestSuite.objects.get(pk=testSuite_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'testSuite not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            if models.TestSuite.objects.filter(~Q(id=testSuite_id), name=name, suite_type=suite_type, product_name_id=product_name_id):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'testSuite already exists'}
                return HttpResponse(jsonify(res))
            testSuite.name = name
            testSuite.product_name_id = product_name_id
            testSuite.suite_type = suite_type
            testSuite.module_name_id = module
            testSuite.owner = owner
            testSuite.level = level
            testSuite.save()
            res = {'code': 0, 'testSuiteName': name}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'testSuite_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def testSuite_add(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        product_name_id = request.POST.get('product_name_id')
        suite_type = request.POST.get('suite_type')
        module = request.POST.get('module_name_id')
        owner = request.POST.get('owner')
        level = request.POST.get('level')
        if str(name).strip() == "" or name is None or \
                str(product_name_id).strip() == "" or product_name_id is None or \
                str(suite_type).strip() == "" or suite_type is None or \
                str(module).strip() == "" or module is None or \
                str(level).strip() == "" or level is None or \
                owner is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if models.TestSuite.objects.filter(name=name, suite_type=suite_type, product_name_id=product_name_id):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'testSuite already exists'}
            return HttpResponse(jsonify(res))
        models.TestSuite.objects.create(
            name=name, product_name_id=product_name_id, suite_type=suite_type, module_name_id=module, owner=owner, level=level)
        res = {'code': 0, 'testSuiteName': name}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


# 编辑测试计划


def testPlan_index(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))

        try:
            testPlans = models.TestPlan.objects.all().order_by('id')
            total = testPlans.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            curentData = Paginator(testPlans, limit).page(page)
            logger.info(testPlans)
            re_data = serializers.serialize("json", curentData)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))

        except Exception as e:
            res = {"code": settings.ERR_TESTPLAN_SELECT,
                   "msg": "testPlans select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,
               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def testPlan_delete(request, testPlan_id):
    if request.method == 'DELETE' or request.method == 'GET':
        try:
            testPlan = models.TestPlan.objects.get(pk=testPlan_id)
            if testPlan:
                logger.debug("delete", testPlan.name)
                models.TestPlan.objects.filter(pk=testPlan_id).delete()
                res = {"code": 0, 'testPlanName': testPlan.name}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                       "msg": "testPlan not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_RESOURCE_NOT_FOUND,
                   "msg": "testPlan not found", 'meta': "%s" % e}
    return HttpResponse(jsonify(res))


def testPlan_edit(request, testPlan_id):
    if request.method == 'POST':
        name = request.POST.get('name')
        suiteNames = request.POST.get('suite_names')
        product_name_id = request.POST.get('product_name_id')
        description = request.POST.get('description')

        if str(name).strip() == "" or name is None or \
                str(product_name_id).strip() == "" or product_name_id is None or \
                str(suiteNames).strip() == "" or suiteNames is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if not product_name_id.isdigit():
            res = {'code': settings.ERR_PARAM,
                   'msg': 'productName parameter type error,must be int !'}
            return HttpResponse(jsonify(res))
        if testPlan_id.isdigit():
            suite_names = ""
            if not suiteNames:
                res = {'code': settings.ERR_PARAM,
                       'msg': 'testSuite dose not exist !'}
                return HttpResponse(jsonify(res))
            else:
                suite_names = suiteNames
            try:
                testPlan = models.TestPlan.objects.get(pk=testPlan_id)
            except Exception as e:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': 'testPlan not found', 'meta': "%s" % e}
                return HttpResponse(jsonify(res))
            if models.TestPlan.objects.filter(~Q(id=testPlan_id), name=name):
                res = {'code': settings.ERR_OBJECT_EXIST,
                       'msg': 'testPlan already exists'}
                return HttpResponse(jsonify(res))
            testPlan.name = name
            testPlan.product_name_id = product_name_id
            testPlan.suite_names = suite_names
            testPlan.description = description
            testPlan.save()
            res = {'code': 0, 'testPlanName': name}
            return HttpResponse(jsonify(res))
        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'testPlan_id parameter error'}
            return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def testPlan_add(request):
    if request.method == 'POST':
        planName = request.POST.get('name')
        suiteIds = request.POST.get('suite_names')
        productNameId = request.POST.get('product_name_id')
        description = request.POST.get('description')
        if str(planName).strip() == "" or planName is None or \
                str(productNameId).strip() == "" or productNameId is None or \
                str(suiteIds).strip() == "" or suiteIds is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        if models.TestPlan.objects.filter(name=planName):
            res = {'code': settings.ERR_OBJECT_EXIST,
                   'msg': 'testPlan already exists'}
            return HttpResponse(jsonify(res))

        if productNameId.isdigit():
            suite_names = ""
            if not suiteIds:
                res = {'code': settings.ERR_PARAM,
                       'msg': 'testSuite dose not exist !'}
                return HttpResponse(jsonify(res))
            else:
                suite_names = suiteIds

        else:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'productName parameter type error,must be int !'}
            return HttpResponse(jsonify(res))

        models.TestPlan.objects.create(
            name=planName, product_name_id=productNameId, suite_names=suite_names, description=description)
        res = {'code': 0, 'testPlanName': planName}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


# test excute
def testPlan_execute(request):
    if request.method == 'POST':
        try:
            productId = request.POST.get('product_name_id')
            planName = request.POST.get('plan_name')
            # "planId" has value when using existed plan;
            #  while it's NULL when choosing temporary test suites.

            suiteIds = request.POST.get('suite_names')
            testHostId = request.POST.get('testhost_ip_id')
            sutId = request.POST.get('sut_ip_id')
            # here we get the release id , but not release name
            releaseId = request.POST.get('release')
            release = models.Release.objects.get(pk=releaseId).name
            browser = request.POST.get('browser')
            osType = request.POST.get('os_type')
        except Exception as e:
            logger.error(e)
            res = {"code": settings.ERR_REQUEST_PARAM_TYPE,
                   "msg": "request param error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))

        if str(productId).strip() == "" or productId is None or \
                str(browser).strip() == "" or browser is None or \
                str(planName).strip() == "" or planName is None or \
                str(suiteIds).strip() == "" or suiteIds is None or \
                str(sutId).strip() == "" or sutId is None or \
                str(testHostId).strip() == "" or testHostId is None or \
                str(osType).strip() == "" or osType is None or \
                str(release).strip() == "" or release is None:
            res = {'code': settings.ERR_PARAM,
                   'msg': 'param cannot be null'}
            return HttpResponse(jsonify(res))
        Time = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3]

        productName = models.Product.objects.filter(
            id=productId).values('name')
        if planName is None or productName is None:
            raise Exception("planName or productName dose not exist")
        testResultId = productName[0]['name'] + \
            '_' + planName + '_' + str(Time)

        if str(sutId) != '0':
            sutIp = models.SUT.objects.filter(id=sutId).values('ip')

        planIds = models.TestPlan.objects.filter(name=planName).values("pk")
        planId = ""
        if planIds:
            planId = planIds[0]["pk"]

        testHostIp = None
        if str(testHostId) != '0':
            testHost = models.TestHost.objects.filter(
                id=testHostId).values('ip')
            testHostIp = testHost[0]['ip']
        total = len(suiteIds.split(','))
        fulfill = 0
        for suiteId in suiteIds.split(','):
            fulfill += 1
            suiteName = models.TestSuite.objects.filter(
                id=suiteId).values('name')
            suiteType = models.TestSuite.objects.filter(
                id=suiteId).values('suite_type')

            res = {'planId': planId,
                   'productId': productId,
                   'productName': productName[0]['name'],
                   'suiteId': suiteId,
                   'suiteType': suiteType[0]['suite_type'],
                   'suiteName': suiteName[0]['name'],
                   'testHostIp': testHostIp,
                   'sutId': sutId,
                   'sutIp': sutIp[0]['ip'],
                   'testResultId': testResultId,
                   'release': release,
                   'browser': browser,
                   'osType': osType,
                   'total': total,
                   'fulfill': fulfill
                   }

            try:
                schedule_task_to_vm.delay(jsonify(res))
            except:
                res = {'code': settings.EXECUTE_FAIL,
                       'msg': 'task excute fail,not found availabel testHost'}
                return HttpResponse(jsonify(res))
        res = {'code': 0,
               'testResultId': testResultId,
               'msg': 'request success'}
        return HttpResponse(jsonify(res))
    else:
        res = {'code': settings.ERR_REQUEST_METHOD_TYPE,
               'msg': 'request method error'}
    return HttpResponse(jsonify(res))


def testPlan_getResult(request):
    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        id = request.GET.get('testResultId')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers' % (limit, page)}
                return HttpResponse(jsonify(res))

        try:
            testResults = models.TestResult.objects.filter(
                testresult_ID=str(id))
            total = testResults.count()
            if str(limit) == '-1':
                if str(total) != '0':
                    limit = total
                else:
                    limit = 1
            curentData = Paginator(testResults, limit).page(page)
            logger.info(testResults)
            re_data = serializers.serialize("json", curentData)
            res = {'code': 0, 'total': total, 'data': re_data}
            return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_TESTPLAN_SELECT,
                   "msg": "testResults select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,

               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def testResult_getAllResult(request):
    browser_map = {
        "0": "IE",
        "1": "Chrome",
        "2": "Firefox",
    }

    if request.method == 'GET':
        limit = request.GET.get('limit')
        page = request.GET.get('page')
        if str(limit) != '-1':
            if not limit.isdigit() or not page.isdigit():
                res = {"code": settings.ERR_PARAM,
                       "msg": 'request param error, limit is %s and page is %s Must all be integers or -1' % (limit, page)}
                return HttpResponse(jsonify(res))
        try:
            query_all_count = models.TestResult.objects.all().count()
            result_all = models.TestResult.objects.all().order_by('-submittime')[(int(page) - 1) * int(limit):(int(page) - 1) * int(limit) + int(limit)].select_related('plan_name',
                                                                                                                                                                        'product_name',
                                                                                                                                                                        'suite_name',
                                                                                                                                                                        'sut_ip',
                                                                                                                                                                        'testhost_ip')
            res_list = []
            for q in result_all:
                q_testresult_ID = q.testresult_ID
                q_release = q.release
                q_passnum = q.passnum
                q_failnum = q.failnum
                q_errornum = q.errornum
                q_starttime = str(q.starttime)
                q_endtime = str(q.endtime)
                q_executetime = str(q.executetime)
                q_browser = browser_map[str(q.browser)]
                plan_name = str(q.plan_name)
                product_name = str(q.product_name)
                suite_name = str(q.suite_name)
                sut_ip = str(q.sut_ip)
                testhost_ip = str(q.testhost_ip)
                report_path = str(q.report_path)
                single_dict = {'testresult_ID': q_testresult_ID,
                               'release': q_release,
                               'passnum': q_passnum,
                               'failnum': q_failnum,
                               'errornum': q_errornum,
                               'starttime': q_starttime,
                               'endtime': q_endtime,
                               'executetime': q_executetime,
                               'browser': q_browser,
                               'plan_name': plan_name,
                               'product_name': product_name,
                               'suite_name': suite_name,
                               'sut_ip': sut_ip,
                               'testhost_ip': testhost_ip,
                               'report_path': report_path
                               }
                res_list.append(single_dict)
            if result_all:
                total = query_all_count
                if str(limit) == '-1':
                    if str(total) != '0':
                        limit = total
                    else:
                        limit = 1
                #currentData = Paginator(res_list, limit).page(page)
                ls = list(res_list)
                re_data = json.dumps(ls)
                res = {'code': 0, 'total': total, 'data': re_data}
                return HttpResponse(jsonify(res))
            else:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': "testResults not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_ALLTESTPLAN_RESULT,
                   "msg": "testResults select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))


def testResult_getTopTenResult(request):

    if request.method == 'GET':
        try:
            all_plan_items = models.TestResult.objects.all().values_list(
                'testresult_ID').distinct().order_by('-submittime')
            if all_plan_items:
                top_ten_item = all_plan_items[0:10]
                res_list = []
                for search_item in top_ten_item:
                    passnum_sum = 0
                    failnum_sum = 0
                    errornum_sum = 0
                    test_result = str(search_item[0])
                    test_plan_results = models.TestResult.objects.filter(
                        testresult_ID=test_result)
                    for q in test_plan_results:
                        passnum_sum += q.passnum
                        failnum_sum += q.failnum
                        errornum_sum += q.errornum
                    single_sum_dict = {'testresult_ID': search_item,
                                       'passnum': passnum_sum,
                                       'failnum': failnum_sum,
                                       'errornum': errornum_sum,
                                       }
                    res_list.append(single_sum_dict)
                re_data = json.dumps(res_list)
                res = {'code': 0, 'data': re_data}
                return HttpResponse(jsonify(res))
            else:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': "testResults not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_TOP_TEN_RESULT,
                   "msg": "testResults select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))


def testResult_getCurrentData(request):
    if request.method == 'GET':
        try:
            uncompletedTestTasksList = []

            testTasks = models.TestResult.objects.values(
                'testresult_ID', 'total',).annotate(fulfill_max=Max('fulfill'))
            for uncompletedTestTask in testTasks:
                if uncompletedTestTask['total'] != uncompletedTestTask['fulfill_max']:
                    uncompletedTestTasksList.append(uncompletedTestTask)

            res_list = []
            productNames = models.Product.objects.values("name")
            if uncompletedTestTasksList:

                for pn in productNames:
                    temp = []
                    for task in uncompletedTestTasksList:
                        resultId = task['testresult_ID']
                        productName = resultId.split('_')[0]

                        total = task['total']
                        fulfill = task['fulfill_max']

                        if productName == pn['name']:
                            task_res = {'resultId': resultId,
                                        'total': total, 'Fulfill': fulfill}
                            temp.append(task_res)

                    if temp:
                        productData = {
                            'productName': pn['name'], 'rate': temp}
                        res_list.append(productData)
                re_data = json.dumps(res_list)
                res = {'code': 0, 'data': re_data}
                return HttpResponse(jsonify(res))
            else:
                res = {"code": settings.NOT_UNCOMPLETED_TASK,
                       "msg": "not found uncompleted task"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_SELECT_CURRENTNUM,
                   "msg": "testResults select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
    else:
        res = {"code": settings.ERR_REQUEST_METHOD_TYPE,

               "msg": 'request method error, plese use get'}
        return HttpResponse(jsonify(res))


def testResult_getSpecificResult(request):

    browser_map = {
        "0": "IE",
        "1": "Chrome",
        "2": "Firefox",
    }
    if request.method == 'GET':
        result_id = request.GET.get('result_id')
        try:
            specific_result = models.TestResult.objects.all().filter(testresult_ID=result_id).order_by('-submittime').select_related('plan_name',
                                                                                                                                     'product_name',
                                                                                                                                     'suite_name',
                                                                                                                                     'sut_ip',
                                                                                                                                     'testhost_ip')
            res_list = []
            for q in specific_result:
                q_testresult_ID = q.testresult_ID
                q_release = q.release
                q_passnum = q.passnum
                q_failnum = q.failnum
                q_errornum = q.errornum
                q_starttime = str(q.starttime)
                q_endtime = str(q.endtime)
                q_executetime = str(q.executetime)
                q_browser = browser_map[str(q.browser)]
                plan_name = str(q.plan_name)
                product_name = str(q.product_name)
                suite_name = str(q.suite_name)
                sut_ip = str(q.sut_ip)
                testhost_ip = str(q.testhost_ip)
                report_path = str(q.report_path)
                single_dict = {'testresult_ID': q_testresult_ID,
                               'release': q_release,
                               'passnum': q_passnum,
                               'failnum': q_failnum,
                               'errornum': q_errornum,
                               'starttime': q_starttime,
                               'endtime': q_endtime,
                               'executetime': q_executetime,
                               'browser': q_browser,
                               'plan_name': plan_name,
                               'product_name': product_name,
                               'suite_name': suite_name,
                               'sut_ip': sut_ip,
                               'testhost_ip': testhost_ip,
                               'report_path': report_path
                               }
                res_list.append(single_dict)
            if specific_result:
                total = specific_result.count()
                ls = list(res_list)
                re_data = json.dumps(ls)
                res = {'code': 0, 'total': total, 'data': re_data}
                return HttpResponse(jsonify(res))
            else:
                res = {'code': settings.ERR_RESOURCE_NOT_FOUND,
                       'msg': "testResults not found"}
                return HttpResponse(jsonify(res))
        except Exception as e:
            res = {"code": settings.ERR_ALLTEST_RESULT,
                   "msg": "testResults select error", 'meta': "%s" % e}
            return HttpResponse(jsonify(res))
