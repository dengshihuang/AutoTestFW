from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Module(models.Model):
    name = models.CharField(max_length=30)
    product_name = models.ForeignKey(Product)

    def __unicode__(self):
        return self.name


class Release(models.Model):
    name = models.CharField(max_length=30)
    product_name = models.ForeignKey(Product)

    def __unicode__(self):
        return self.name


class TestSuite(models.Model):
    name = models.CharField(max_length=60)
    product_name = models.ForeignKey(Product)
    suite_type = models.CharField(max_length=3)
    module_name = models.ForeignKey(Module)
    owner = models.CharField(max_length=50)
    level = models.CharField(max_length=15)

    def __unicode__(self):
        return self.name


class TestHost(models.Model):
    ip = models.GenericIPAddressField()
    user = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    status = models.CharField(max_length=10)
    use_by = models.CharField(max_length=30, null=True, blank=True)
    os_type = models.CharField(max_length=15)

    def __unicode__(self):
        return self.ip


class SUT(models.Model):
    ip = models.GenericIPAddressField()
    user = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    status = models.CharField(max_length=10)
    use_by = models.CharField(max_length=30, null=True, blank=True)
    os_type = models.CharField(max_length=15)

    def __unicode__(self):
        return self.ip


class TestPlan(models.Model):
    name = models.CharField(max_length=40)
    product_name = models.ForeignKey(Product)
    description = models.CharField(max_length=256, null=True, blank=True)
    suite_names = models.CharField(max_length=1200)

    def __unicode__(self):
        return self.name


class TestResult(models.Model):
    testresult_ID = models.CharField(max_length=60)
    submittime = models.CharField(max_length=13)
    release = models.CharField(max_length=8)
    plan_name = models.ForeignKey(TestPlan, null=True)
    suite_name = models.ForeignKey(TestSuite)
    passnum = models.IntegerField()
    failnum = models.IntegerField()
    errornum = models.IntegerField()
    starttime = models.DateTimeField()
    endtime = models.DateTimeField()
    executetime = models.DecimalField(max_digits=7, decimal_places=2)
    testhost_ip = models.ForeignKey(TestHost)
    sut_ip = models.ForeignKey(SUT)
    browser = models.CharField(max_length=15)
    total = models.IntegerField(default=0)
    fulfill = models.IntegerField(default=0)
    product_name = models.ForeignKey(Product)
    report_path = models.CharField(default='', max_length=100)

    def __unicode__(self):
        return self.testresult_ID
