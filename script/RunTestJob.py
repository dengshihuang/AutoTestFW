# coding= utf-8
import os
import sys
import json
import psutil
import threading

from SimpleXMLRPCServer import SimpleXMLRPCServer
from SocketServer import ThreadingMixIn

from NeoTestRunner import NeoTestRunner

if os.name == "nt":
    LOG_DEFAULT_DIR = "d:\\mnt\\log"
else:
    LOG_DEFAULT_DIR = "/mnt/logdir/" 


class ThreadXMLRPCServer(ThreadingMixIn,SimpleXMLRPCServer): 
    pass

class RunTestJob:
    def __init__(self, curPath=None):
        self.curPath = curPath  #the path of this class file
        
    def get_cpu(self):
        cpuPercent = psutil.cpu_percent(1)
        return cpuPercent
    
    def keep_alive(self):
        return 'alive'
    
    def run_job(self, msgJson):
        """
        'planName':planName,
        'productName':productName,
        'description':description,
        'suiteName':suiteName,
        'testHostIp':testHostIp,
        'sutIp':sutIp,
        'testResultId':testResultId,
        'release':release,
        'browser':browser,
        'osType':osType,
        'suiteType':suiteType
  
        return json {passNum,failNum,errorNum,startTime,endTime,excuteTime}
        """    
        msgConf = {}
        result = None
        
        if msgJson is not None:
            msgConf = json.loads(msgJson)
        print(msgJson)
        msgConf['curPath'] = self.curPath
        
        reportDir = os.sep.join([LOG_DEFAULT_DIR , msgConf['productName'], msgConf['testResultId']])
        if not os.path.exists(reportDir): 
            os.makedirs(reportDir)
        else:
            if not os.path.isdir(reportDir):
                os.remove(reportDir)
                os.makedirs(reportDir)
        
        reportFile = (os.sep).join([reportDir, msgConf['suiteName']+'.html' ])
        with open(reportFile, 'wb') as fp:
            neoRunner = NeoTestRunner(msgConf, stream = fp)
            suite = neoRunner.get_suite()
            result = neoRunner.get_result(suite)
            result['reportPath'] = os.sep.join([msgConf['productName'], msgConf['testResultId'], msgConf['suiteName']]) 
            
        return  json.dumps(result)     #result   

path = os.path.realpath(sys.argv[0]) 
if os.path.isfile(path):  
    path=os.path.dirname(path)  

obj = RunTestJob(os.path.abspath(path))
 
myIP = 'localhost'
if len(sys.argv) >=2:
  myIP = sys.argv[1]

server = ThreadXMLRPCServer((myIP, 8008), allow_none=True)
server.register_instance(obj)

print "Listening on port 8008"
server.serve_forever()
