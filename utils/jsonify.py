import json
import datetime
from django.utils.timezone import get_current_timezone


def convertTimeZone(obj):
    return obj.astimezone(get_current_timezone())

class JSONIFYEncoder(json.JSONEncoder):
    def default(self, obj):
        self.ensure_ascii = False 
        if isinstance(obj, datetime.datetime):
            obj = convertTimeZone(obj)
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(obj, datetime.time):
            return obj.strftime("%H:%M:%S")
        elif isinstance(obj, datetime.date):
            return obj.strftime("%Y:%m:%d")
        elif isinstance(obj, datetime.timedelta):
            return str(obj)
        elif isinstance(obj, set):
            return list(obj)

        return super(JSONIFYEncoder, self).default(obj)


jsonify = JSONIFYEncoder().encode
