# -*- coding: utf-8 -*-
import xmlrpclib,httplib
import smtplib
from email.mime.text import MIMEText
import json

class TimeoutTransport(xmlrpclib.Transport):
    timeout = 10.0

    def set_timeout(self, timeout):
        self.timeout = timeout

    def make_connection(self, host):
        h = httplib.HTTPConnection(host, timeout=self.timeout)
        return h


mail_host = "smtp.huayun.com"
mail_to = "wangzhengxian@huayun.com"
mail_from = "automation_test@huayun.com"
mail_pass = "Arrisultra#test1"


def mail_conn_error(error_ip):
    msg = MIMEText("Host %s has connection error!\nPlease fix the issue ASAP. " % error_ip)
    msg['Subject'] = "Network Error For Host %s" % error_ip
    msg['From'] = mail_from
    msg['To'] = mail_to
    try:
        s = smtplib.SMTP()
        s.connect(mail_host, "25")
        s.starttls()
        s.login(mail_from,mail_pass)
        s.sendmail(mail_from, mail_to, msg.as_string())
        s.quit()
    except Exception, e:
        print str(e)


def mail_new_job(jobMsg):
    testResultId = str(jobMsg["testResultId"])
    planId = str(jobMsg["planId"])
    productName = str(jobMsg["productName"])
    suiteName = str(jobMsg["suiteName"])
    suiteType = str(jobMsg["suiteType"])
    testHostIp = str(jobMsg["testHostIp"])
    sutIp = str(jobMsg["sutIp"])
    release = str(jobMsg["release"])
    browser = str(jobMsg["browser"])
    text = "Job Result ID: " + testResultId +\
        "\nplanId: " + planId +\
        "\nproductName: " + productName +\
        "\nsuiteName: " + suiteName +\
        "\nsuiteType: " + suiteType +\
        "\ntestHostIp: " + testHostIp +\
        "\nsutIp: " + sutIp +\
        "\nrelease: " + release +\
        "\nbrowser: " + browser

    msg = MIMEText(text)
    msg['Subject'] = "New job %s has been sent. " % jobMsg["testResultId"]
    msg['From'] = mail_from
    msg['To'] = mail_to
    try:
        s = smtplib.SMTP()
        s.connect(mail_host, "25")
        s.starttls()
        s.login(mail_from,mail_pass)
        s.sendmail(mail_from, mail_to, msg.as_string())
        s.quit()
    except Exception, e:
        print str(e)